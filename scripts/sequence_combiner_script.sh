# sequence_combiner script for combining individual npz files in a given folder into run_combined.npz files with shuffling

# important argument
# imdata_name: used to give the name of folder under root folder which contains the individual npz files in a npdir folder
#							 you can choose to use list token instead. See below for description

# other arguments which can be changed
# --npzdir, type=str, default = "rootpath/imdata_name/npzdir",
# 										 help="enter the npz file to find the npzfiles"
# --distdir, type=str, default = "rootpath/imdata_name/Distance",
# 										 help="folder which has the distance fiels from webots"
# --statedir, type=str, default = "rootpath/imdata_name/State",
# 										 help="folder which has the state information from webots"
# --img_coll_dir, type=str, default="rootpath/imdata_name/Coll",
# 										 help="folder that stores all the images for Collision from webots"
# --img_nocoll_dir, type=str, default="rootpath/imdata_name/NoColl",
# 										 help="folder that stores all the images for no-collision from webots"
# --nb_states, type=int, default=17,
# 										 help="input for number of states extracted from webots"
# --seq_length, type=int, default=5,
# 										 help="input for mentioning the output sequence length or frames per sequence"

# mandatory bash script tokens
# list: give the list of names of foldes in the root path which has the raw data from webots
# Example format:
# list='rawdata_folder1 rawdata_folder2 rawdata_folder3'

for i in $list; do
	echo "$(realpath "$i")"
	python3 sequence_combiner.py --imdata_name=$(realpath "$i")

done

# train_script
#
# FLAG details
#
# lr: learning rate for optimizer
# batch_size: number of samples per step or batch_size
# num_epochs: number of epochs for training
# base_epoch: variable to store the base epoch used to continue training from specific epoch
# epochs_per_chkpt: variable to mention number of epochs per each checkpoint; give -1 to save a single checkpoint with max accuracy
# max_to_keep: variable to mention the max number of latest checkpoints to keep, the older chkpts will be removed; give -1 for no limit
#
# steps_verify: number of training steps between verification
# DISPLAY_STEP: number of global steps for each summary step
# LOG_DEVICE_PLACEMENT: flag to display all the gpu and hardware info for a tf session
# seq_length: number of frames in sequence
# shuffle_buffer_size: shuffle buffer while parsing data from tfrecords file
# prefetch_buffer_size: prefetch shuffle buffer while parsing data from tfrecords file
# number_parallel_calls: num of threads to initialize in tf.dataset.map call for processing tfrecord string
# dense_drop: dropout rate for dense layer
# mc_dropout_rate: dropout rate for MC dropout for ConvLSTM layers
# image_pipeline_mode: image pippeline mode to define number of image channels to network
# isa_enable: flag to enable or disable state and action channel along with image pipeline
# SFP_enable: flag to enable or disable stochastic forward pass
# timeline_enable: flag to enable or disable timmeprofiling of network training
#
# check ./src/flags_initialize.py for flags to change default training data directories and other flags with descriptions

python3 ../src/train_val.py --lr=0.0001 --batch_size=32 --num_epochs=5 --base_epoch=0 --epochs_per_chkpt=3 --max_to_keep=3 --steps_verif=10 --DISPLAY_STEP=5 --LOG_DEVICE_PLACEMENT='False' \
			 --seq_length=5 --shuffle_buffer_size=100 --prefetch_buffer_size=100 --num_parallel_calls=4 --dense_drop=0.3 --mc_dropout_rate=0.05 --image_pipeline_mode=3 \
			 --isa_enable='True' --SFP_enable='False' --timeline_enable='False'  #> stdout-dpm.txt 2>&1

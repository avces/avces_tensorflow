# test_script
#
# FLAG details
#
# inference: flag to toggle between test mode and inference mode; True for test mode uses the *_test.tfrecords file in tfrecords_dir;
# 						False for inference mode uses the "inf_npzfile"	flag defined in src/flags_initialize.py
# IMPORTANT --> restore_model_file: checkpoint file to load the model for test/inference
# 							default = rootpath/chkpt_dir/run1/pretrained_model; give your own location for a different checkpoint file
# Ex input: "$rootpath/chkpt_dir/run1/model_epoch_6" or "$rootpath/chkpt_dir/run1/model_maxaccu_epoch_6_123-123"; check your chkpt file location
#
# IMPORTANT --> inf_npzfile: used when inference=True; npz format file location which has the image sequences to perform inference on
# 						default= root_path/infdata/npzdir/inference_data.npz;
# 						make sure that a file with given name is available in the default location or give your file location

# batch_size: number of samples per step or batch_size
# LOG_DEVICE_PLACEMENT: flag to display all the gpu and hardware info for a tf session
# seq_length: number of frames in sequence
# shuffle_buffer_size: shuffle buffer while parsing data from tfrecords file
# prefetch_buffer_size: prefetch shuffle buffer while parsing data from tfrecords file
# number_parallel_calls: num of threads to initialize in tf.dataset.map call for processing tfrecord string
# dense_drop: dropout rate for dense layer
# mc_dropout_rate: dropout rate for MC dropout for ConvLSTM layers
# image_pipeline_mode: image pippeline mode to define number of image channels to network
# isa_enable: flag to enable or disable state and action channel along with image pipeline
# SFP_enable: flag to enable or disable stochastic forward pass
#
# check ../src/flags_initialize.py for more flags and descriptions

# for inference mode the predicted values and corresponding descriptions are also stored as a .npy file in the directory of inf_npzfile location

python3 ../src/test.py --batch_size=32 --LOG_DEVICE_PLACEMENT=False --seq_length=5 --shuffle_buffer_size=100 --prefetch_buffer_size=100 --num_parallel_calls=4 \
	--dense_drop=0.3 --mc_dropout_rate=0.05 --image_pipeline_mode=3 --isa_enable=True --SFP_enable=True --labels_sec=5  --inference=False \
	# check above description for --inf_npzfile and --restore_model_file #> stdout-test-sfp-dpm.txt 2>&1

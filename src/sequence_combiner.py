#
import os
import numpy as np
import time
import shutil
import tensorflow as tf
import sys
import argparse
import random
#

def combine_npz_from_dir(newfile, npzdir, nb_states):
    # get contents of directory and sort
    dircontents = sorted(os.listdir(npzdir))
    #
    # init first_file_flag
    first_file_flag = 1
    # loop thru files in dir
    #sess = tf.Session()
    for filename in dircontents:
        print("working on file: ", filename)
        fparts = filename.split('.')
        #
        # skip any file like "run_combined.npz"
        #
        if fparts[0] == 'run_combined':
            print("skipping file ", filename)
            continue

        if fparts[-1] == 'npz':
            if first_file_flag == 1:
                # get arrays from file
                f = np.load(os.path.join(npzdir, filename))
                # init arrays
                num_examples = f['num_examples']
                seq_images1 = np.array(f['seq_images1'])
                seq_images2 = np.array(f['seq_images2'])
                seq_images3 = np.array(f['seq_images3'])
                seq_states = np.array(f['seq_states'])
                seq_statesnoa = np.array(f['seq_statesnoa'])
                seq_labels1 = np.array(f['seq_labels1'])
                seq_labels2 = np.array(f['seq_labels2'])
                seq_labels3 = np.array(f['seq_labels3'])
                seq_labels4 = np.array(f['seq_labels4'])
                seq_labels5 = np.array(f['seq_labels5'])
                seq_descriptions = np.array(f['seq_descriptions'])
                # reset first_file_flag
                first_file_flag = 0
            else:
                # get arrays from file
                f = np.load(os.path.join(npzdir, filename))
                # get array and put in temp; then add/concat to existing array
                #  do this for each array in file
                temp_num_examples = f['num_examples']
                num_examples = num_examples + int(temp_num_examples)
                seq_images1 = np.concatenate((seq_images1, f['seq_images1']), axis=0)
                seq_images2 =np.concatenate((seq_images2, f['seq_images2']), axis=0)
                seq_images3 = np.concatenate((seq_images3, f['seq_images3']), axis=0)
                seq_states = np.concatenate((seq_states, f['seq_states']), axis=0)
                seq_statesnoa = np.concatenate((seq_statesnoa, f['seq_statesnoa']), axis=0)
                seq_labels1 = np.concatenate((seq_labels1, f['seq_labels1']), axis=0)
                seq_labels2 = np.concatenate((seq_labels2, f['seq_labels2']), axis=0)
                seq_labels3 = np.concatenate((seq_labels3,f['seq_labels3']), axis=0)
                seq_labels4 = np.concatenate((seq_labels4, f['seq_labels4']), axis=0)
                seq_labels5 = np.concatenate((seq_labels5, f['seq_labels5']), axis=0)
                seq_descriptions = np.concatenate((seq_descriptions, f['seq_descriptions']), axis=0)
            #  end of if/else
        # end of if npz
    # end of for loop
    #
    # check support


    zipped = list(zip(seq_images1, seq_images2, seq_images3, seq_labels1, seq_labels2,\
                        seq_labels3, seq_labels4, seq_labels5, seq_descriptions, seq_states, seq_statesnoa))
    random.seed(450)
    random.shuffle(zipped)
    random.shuffle(zipped)
    random.shuffle(zipped)

    seq_images1, seq_images2, seq_images3, seq_labels1, seq_labels2,\
    seq_labels3, seq_labels4, seq_labels5, seq_descriptions, seq_states, seq_statesnoa = list(zip(*zipped))

    seq_images1 = np.array(seq_images1)
    seq_images2 = np.array(seq_images2)
    seq_images3 = np.array(seq_images3)
    seq_labels1 = np.array(seq_labels1)
    seq_labels2 = np.array(seq_labels2)
    seq_labels3 = np.array(seq_labels3)
    seq_labels4 = np.array(seq_labels4)
    seq_labels5 = np.array(seq_labels5)
    seq_descriptions = np.array(seq_descriptions)
    seq_states = np.array(seq_states)
    seq_statesnoa = np.array(seq_statesnoa)

    count_nocoll = 0
    count_coll = 0
    for i in range(num_examples):
        if seq_labels5[i, 1] == 0:
            count_nocoll += 1
        else:
            count_coll += 1
    #
    print("support using labels1:")
    print("count_nocoll: ", count_nocoll)
    print("count_coll: ", count_coll)
    #
    # save to new combined file
    if nb_states != 0:
        np.savez(os.path.join(npzdir,newfile), num_examples=num_examples, \
            seq_images1=seq_images1, seq_images2=seq_images2, seq_images3=seq_images3, \
            seq_states=seq_states, seq_statesnoa=seq_statesnoa, \
            seq_labels1=seq_labels1, seq_labels2=seq_labels2, seq_labels3=seq_labels3, \
            seq_labels4=seq_labels4, seq_labels5=seq_labels5, \
            seq_descriptions=seq_descriptions)
    else:
        np.savez(os.path.join(npzdir,newfile), num_examples=num_examples, \
            seq_images1=seq_images1, seq_images2=seq_images2, seq_images3=seq_images3, \
            seq_labels1=seq_labels1, seq_labels2=seq_labels2, seq_labels3=seq_labels3, \
            seq_labels4=seq_labels4, seq_labels5=seq_labels5, \
            seq_descriptions=seq_descriptions)
# end of combine_npz_from_dir

# main
if __name__ == "__main__":
    #
    parser = argparse.ArgumentParser(description = "parser to parse all input directories for sequence_combiner")

    cwd = os.getcwd()
    root_path = os.path.normpath(os.getcwd() + os.sep + os.pardir)

    parser.add_argument('--imdata_name', type=str, default=' ',
                             help="enter the npz folder to fetch individual sequences generated from seq_builder")
    args = parser.parse_args()

    imdata_path = os.path.join(root_path, args.imdata_name)

    if os.path.isdir(imdata_path) == False or len(os.listdir(imdata_path)) == 0:
        print("the raw image data folder imdata does not exist or is empty; Please refer to webots link in readme to create/fill the folder")

    parser.add_argument('--npzdir', type=str, default=os.path.join(imdata_path, "npzdir"),
                         help="enter the npz folder to fetch individual sequences generated from seq_builder")
    parser.add_argument('--combined_file', type=str, default="run_combined.npz",
                         help="enter the name of combined file in .npz format")
    parser.add_argument('--nb_states', type=int, default=17,
                     help="input for number of states extracted from webots")

    args = parser.parse_args()

    if os.path.isdir(args.npzdir) == False or len(os.listdir(args.npzdir))==0:
        print("The npzdir doesnot exist or is empty; exiting...")
        sys.exit(0)
    #
    # start the clock
    start_time = time.time()
    # do some work

    # combine the npz files
    print(" ")
    print("combining npz files")
    combine_npz_from_dir(args.combined_file, args.npzdir, args.nb_states)
    #
    # stop the clock
    elap_time = time.time() - start_time
    # report the elapsed time
    print('Elapsed time: %.2f seconds' % (elap_time))

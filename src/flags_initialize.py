import argparse
import os

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def define_flags():

       parser = argparse.ArgumentParser(description = "parser to parse all the specifications of network")

       cwd = os.getcwd()
       root_path = os.path.normpath(os.getcwd() + os.sep + os.pardir)

       # ALL THE REQUIRED DIRECTORY LOCATIONS
       parser.add_argument('--chkpt_dir', type= str, default=os.path.join(root_path,"chkpt_dir"),
                                   help="""dir to store trained network parameters""")
       parser.add_argument('--filewriter_dir', type= str, default=os.path.join(root_path, "filewriter_dir"),
                                   help="""dir to store summaries""")
       parser.add_argument('--tfrecords_dir', type= str, default=os.path.join(root_path, "tfrecords_dir"),
                                   help="""dir containing the .tfrecords train, val and test data""")
       parser.add_argument('--timeline_dir', type= str, default=os.path.join(root_path, "timeline_dir"),
                                   help="""dir to store the time profile info""")
       parser.add_argument('--infdata_dir', type= str, default=os.path.join(root_path, "infdata"),
                                   help="""dir to store the time profile info""")

       parser.add_argument('--run_num', type= int, default=1,
                                   help=""" run number of training""")
       parser.add_argument('--tfrecord_details_file', type= str, default="tfrecord_data_details.txt",
                                   help="""file name in tfrecords directory which contains details about no:f samples""")
       parser.add_argument('--tfrecord_coll_info', type= str, default="tfrecord_data_coll_details.txt",
                                   help="""file name in tfrecords directory which contains details about collision and non collision samples""")
       parser.add_argument('--restore_model_file', type= str, default=os.path.join(root_path, "chkpt_dir", "run_pretrained", "pretrained_model"),
                                   help="""checkpoint file to restore a trained model""")

       # TRAINING SPECS
       parser.add_argument('--lr', type= float, default=.0001,
                                   help="""learning rate for optimizer""")
       parser.add_argument('--batch_size', type= int, default=32,
                                   help="""batch size for training""")
       parser.add_argument('--num_epochs', type= int, default=5,
                                   help="""number of epochs to train each fold""")
       parser.add_argument('--labels_sec', type= int, default=2,
                                   help=""" which labels to use ex: 1 for labels1 which annotates as coll 1sec before collsion """)
       parser.add_argument('--steps_verif', type= int, default=10,
                                   help="""number of training steps between verification""")
       parser.add_argument('--early_stop_patience', type= int, default=10,
                                   help="""early stopping patience""")
       parser.add_argument('--DISPLAY_STEP', type= int, default=5,
                                   help="""number of global steps for each summary step""")
       parser.add_argument('--LOG_DEVICE_PLACEMENT', type= str2bool, default=False,
                                   help="""flag to display all the gpu and hardware info for a tf session""")
       parser.add_argument('--restore_model', type= str2bool, default=False,
                                   help="""flag to restore the previously trained model ;True if enabled""")
       parser.add_argument('--base_epoch', type= int, default=0,
                                   help="""variable to store the base epoch used to continue training from specific epoch""")
       parser.add_argument('--max_to_keep', type= int, default=5,
                                   help="""variable to mention the max number of latest checkpoints to keep, the older chkpts will be removed; give -1 for no limit""")
       parser.add_argument('--epochs_per_chkpt', type= int, default=3,
                                   help="""variable to mention number of epochs per each checkpoint; give -1 to save a single checkpoint with max accuracy""")

       # INPUT DATA SPECS
       parser.add_argument('--nb_classes', type= int, default=2,
                                   help="""number of classes""")
       parser.add_argument('--num_states', type= int, default=17,
                                   help="""number of input states and actions""")
       parser.add_argument('--num_states_noa', type= int, default=16,
                                   help="""number of input states without actions""")
       parser.add_argument('--image_rows', type= int, default=256,
                                   help="""number of rows in input images""")
       parser.add_argument('--image_cols', type= int, default=256,
                                   help="""number of columns in input images""")
       parser.add_argument('--image_chans', type= int, default=1,
                                   help="""number of channels in input images""")
       parser.add_argument('--seq_length', type= int, default=5,
                                   help="""number of frames in sequence""")

       #INPUT PIPELINE SPECS
       parser.add_argument('--shuffle_buffer_size', type= int, default=100,
                                   help="""shuffle buffer while parsing data from tfrecords file""")
       parser.add_argument('--prefetch_buffer_size', type= int, default=100,
                                   help="""prefetch shuffle buffer while parsing data from tfrecords file""")
       parser.add_argument('--num_parallel_calls', type= int, default=4,
                                   help="""num of threads to initialize in tf.dataset.map call for processing tfrecord string""")

       # NETWORK SPECS
       parser.add_argument('--dense_drop', type= float, default=0.3,
                                   help="""dropout rate for dense layer""")
       parser.add_argument('--mc_dropout_rate', type= float, default=0.05,
                                   help="""dropout rate for MC dropout for ConvLSTM layers""")
       parser.add_argument('--image_pipeline_mode', type= int, default=3,
                                    help="""image pippeline mode to define number of image channels to network""")
       parser.add_argument('--isa_enable', type= str2bool, default=True,
                                   help="""flag to enable or disable state and action channel along with image pipeline""")
       parser.add_argument('--vis_enable', type= str2bool, default=False,
                                   help="""flag to enable or disable vis mode to store intermediate outputs from convlstm layers of image pipeline""")
       parser.add_argument('--SFP_enable', type= str2bool, default=False,
                                   help="""flag to enable or disable stochastic forward pass""")
       parser.add_argument('--timeline_enable', type= str2bool, default=True,
                                   help="""flag to enable or disable time profiling""")

       # Flags related to test script
       parser.add_argument('--inference', type= str2bool, default=False,
                                   help="""flag to switch between evaluation and inference mode of test file; default False==evaluation""")
       parser.add_argument('--inf_npzfile', type= str, default=os.path.join(root_path, "infdata", "npzdir", "inference_data.npz"),
                                   help="""npz format file location which has the image sequences to perform inference on""")

       FLAGS = parser.parse_args()

       if os.path.isdir(FLAGS.chkpt_dir) == False:
              os.mkdir(FLAGS.chkpt_dir)
       if os.path.isdir(FLAGS.filewriter_dir) == False:
              os.mkdir(FLAGS.filewriter_dir)
       if os.path.isdir(FLAGS.timeline_dir) == False:
              os.mkdir(FLAGS.timeline_dir)

       if os.path.isdir(os.path.join(FLAGS.chkpt_dir, "run{}".format(str(FLAGS.run_num)))) == False:
              os.mkdir(os.path.join(FLAGS.chkpt_dir, "run{}".format(str(FLAGS.run_num))))
       if os.path.isdir(os.path.join(FLAGS.filewriter_dir, "run{}".format(str(FLAGS.run_num)))) == False:
              os.mkdir(os.path.join(FLAGS.filewriter_dir, "run{}".format(str(FLAGS.run_num))))
       if os.path.isdir(os.path.join(FLAGS.timeline_dir, "run{}".format(str(FLAGS.run_num)))) == False:
              os.mkdir(os.path.join(FLAGS.timeline_dir, "run{}".format(str(FLAGS.run_num))))

       return FLAGS

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os
import sys
import numpy as np
import tensorflow as tf
import random

parser = argparse.ArgumentParser()

cwd = os.getcwd()
root_path = os.path.normpath(os.getcwd() + os.sep + os.pardir)


parser.add_argument('--npzdir', type=str, default=os.path.join(root_path, "imdata","npzdir"),
						 help="enter the location of npz directory to generate the tfrecords file from")
parser.add_argument('--name', type=str, default="dataset",
						 help="enter the root name of tfrecords files")
parser.add_argument('--tfrecords_dir', type=str,default= os.path.join(root_path, "tfrecords_dir"),
						 help="enter the directory to store the tfrecords files")

args = parser.parse_args()

if os.path.isdir(args.npzdir) == False:
	print("npzdir", args.npzdir,"does not exist")
	sys.exit(0)
if os.path.isdir(args.tfrecords_dir) == False:
	os.mkdir(args.tfrecords_dir)
if os.path.isdir(os.path.join(args.tfrecords_dir,"train")) == False:
	os.mkdir(os.path.join(args.tfrecords_dir,"train"))
if os.path.isdir(os.path.join(args.tfrecords_dir,"validate")) == False:
	os.mkdir(os.path.join(args.tfrecords_dir,"validate"))
if os.path.isdir(os.path.join(args.tfrecords_dir,"test")) == False:
	os.mkdir(os.path.join(args.tfrecords_dir,"test"))


def _int64_feature(value):
	return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def _bytes_feature(value):
	return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def convert_to():

	load_data = np.load(os.path.join(args.npzdir,"run_combined.npz"))
	data = {}

	for f in load_data.files:
		if f == 'num_examples':
			num_examples = load_data['num_examples']
			continue
		else:
			data[f] = load_data[f]

	zipped = list(zip(*data.values()))
	random.seed(450)
	random.shuffle(zipped)
	random.shuffle(zipped)
	random.shuffle(zipped)

	zip_seperated = list(zip(*zipped))
	keys = list(data.keys())

	for i in range(len(keys)):
		data[keys[i]] = np.array(zip_seperated[i])

	for key in keys:
		print(key, data[key].shape)

	rows = data['seq_images1'].shape[2]
	cols = data['seq_images1'].shape[3]
	seq_length = data['seq_images1'].shape[1]

	# check to make sure num_examples is correct
	if data['seq_images1'].shape[0] != num_examples:
		raise ValueError('Number of images/sequences %d does not match num_examples %d.' %
						 (data['seq_images1'].shape[0], num_examples))

	if len(data['seq_images1'].shape) == 4:
		channels = 1
	else:
		channels = data['seq_images1'].shape[4]

	# filename = os.path.join(FLAGS.rec_dir, name + '.tfrecords')
	train_filename = os.path.join(args.tfrecords_dir, "train", args.name + '_train.tfrecords')
	val_filename = os.path.join(args.tfrecords_dir, "validate", args.name + '_val.tfrecords')
	test_filename = os.path.join(args.tfrecords_dir, "test", args.name + '_test.tfrecords')

	print('Writing\n', train_filename, "\n", val_filename,"\n", test_filename)

	train_writer = tf.python_io.TFRecordWriter(train_filename)
	val_writer = tf.python_io.TFRecordWriter(val_filename)
	test_writer = tf.python_io.TFRecordWriter(test_filename)

	train_ex = 0
	val_ex = 0
	test_ex = 0
	select_from = ['train', 'val', 'test']
	
	#train, validation and test data splits respectively; change the values below and make sure their sum equals 1
	p = [0.77, 0.15, 0.08]
	coll_sample = 0
	no_coll_sample = 0
	coll_train = 0
	coll_validate = 0
	coll_test = 0
	coll = False

	for index in range(num_examples):

		image1_raw = data['seq_images1'][index].tostring()
		image2_raw = data['seq_images2'][index].tostring()
		image3_raw = data['seq_images3'][index].tostring()

		label1_raw = data['seq_labels1'][index]
		if label1_raw[0] == 0:
			coll_sample += 1
			coll = True
		else:
			no_coll_sample += 1
			coll = False

		label1_raw = label1_raw.tostring()
		label2_raw = data['seq_labels2'][index].tostring()
		label3_raw = data['seq_labels3'][index].tostring()
		label4_raw = data['seq_labels4'][index].tostring()
		label5_raw = data['seq_labels5'][index].tostring()
		stateinfo_raw = data['seq_states'][index].tostring()
		statenoa_raw = data['seq_statesnoa'][index].tostring()

		example = tf.train.Example(features=tf.train.Features(feature={
				'num_examples' : _int64_feature(num_examples),
				'seq_length': _int64_feature(seq_length),
				'rows': _int64_feature(rows),
				'cols': _int64_feature(cols),
				'channels': _int64_feature(channels),
				'label1_raw': _bytes_feature(tf.compat.as_bytes(label1_raw)),
				'label2_raw': _bytes_feature(tf.compat.as_bytes(label2_raw)),
				'label3_raw': _bytes_feature(tf.compat.as_bytes(label3_raw)),
				'label4_raw': _bytes_feature(tf.compat.as_bytes(label4_raw)),
				'label5_raw': _bytes_feature(tf.compat.as_bytes(label5_raw)),
				'image1_raw': _bytes_feature(tf.compat.as_bytes(image1_raw)),
				'image2_raw': _bytes_feature(tf.compat.as_bytes(image2_raw)),
				'image3_raw': _bytes_feature(tf.compat.as_bytes(image3_raw)),
				'stateinfo_raw': _bytes_feature(tf.compat.as_bytes(stateinfo_raw)),
				'statenoa_raw': _bytes_feature(tf.compat.as_bytes(statenoa_raw))
		}))

		choice = np.random.choice(select_from, 1, p=p)
		if choice == 'train':
			train_writer.write(example.SerializeToString())
			train_ex += 1
			if coll == True:
				coll_train += 1
		elif choice == 'val':
			val_writer.write(example.SerializeToString())
			val_ex += 1
			if coll == True:
				coll_validate += 1
		elif choice == 'test':
			test_writer.write(example.SerializeToString())
			test_ex += 1
			if coll == True:
				coll_test += 1

	with open(os.path.join(args.tfrecords_dir, "tfrecord_data_details.txt"), "w") as f:
		f.write("training %d\nvalidation %d\ntest %d" % (train_ex, val_ex, test_ex))

	with open(os.path.join(args.tfrecords_dir, "tfrecord_data_coll_details.txt"), "w") as f:
		f.write("total_collision_samples %d\ntotal_no_coll_samples %d\ntrain_coll %d\ntrain_nocoll %d\nval_coll %d\nval_nocoll %d\ntest_coll %d\ntest_nocoll %d" % \
							(coll_sample, no_coll_sample, coll_train, train_ex-coll_train, coll_validate, val_ex-coll_validate, coll_test, test_ex-coll_test))
	train_writer.close()
	val_writer.close()
	test_writer.close()

if __name__ == '__main__':
	convert_to()

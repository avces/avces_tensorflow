import numpy as np
import os

class getData():
    def __init__(self, FLAGS):
        self.FLAGS = FLAGS
        self.get_dataset(FLAGS)

    def shuffle_mes(self,seq_images1, seq_images2, seq_images3, seq_states, seq_statesnoa, seq_descriptions, seq_labels=None):
        numex = np.shape(seq_images1)[0]    # get the number of examples
        list_indices_orig = range(numex)    # get the original list of indices
        # shuffle using np.random.permutation
        list_indices_shuf = np.random.permutation(list_indices_orig)
        for j in range(20):
            list_indices_shuf = np.random.permutation(list_indices_shuf)
        #
        # apply the shuffle to the data
        seq_images1 = np.take(seq_images1, list_indices_shuf, axis=0)
        seq_images2 = np.take(seq_images2, list_indices_shuf, axis=0)
        seq_images3 = np.take(seq_images3, list_indices_shuf, axis=0)
        seq_states = np.take(seq_states, list_indices_shuf, axis=0)
        seq_statesnoa = np.take(seq_statesnoa, list_indices_shuf, axis=0)
        seq_descriptions = np.take(seq_descriptions, list_indices_shuf, axis=0)
        #
        # return the results
        return seq_images1, seq_images2, seq_images3, seq_states,seq_statesnoa, seq_descriptions

    def get_dataset(self, FLAGS):
        filename = FLAGS.inf_npzfile
        print("running inference on ", filename)
        with np.load(filename) as big_datafile:
            self.num_examples = int(big_datafile['num_examples'])
            seq_images1 = big_datafile['seq_images1']
            seq_images2 = big_datafile['seq_images2']
            seq_images3 = big_datafile['seq_images3']
            seq_states = big_datafile['seq_states']
            seq_statesnoa = big_datafile['seq_statesnoa']
            seq_descriptions = big_datafile['seq_descriptions']

            seq_images1 = seq_images1[..., np.newaxis]
            seq_images2 = seq_images2[..., np.newaxis]
            seq_images3 = seq_images3[..., np.newaxis]

            self.seq_images1, self.seq_images2, self.seq_images3, self.seq_states, self.seq_statesnoa, self.seq_descriptions = \
                self.shuffle_mes(seq_images1, seq_images2, seq_images3, seq_states, seq_statesnoa, seq_descriptions)

            self.ipos = 0
            self.batch_size = FLAGS.batch_size
            self.complete_flag = 0

    def get_next_batch(self):
        ini_index = self.ipos

        if self.ipos+self.batch_size >= self.num_examples :
            final_index = self.num_examples-1
            self.complete_flag = 1
        else:
            final_index = self.ipos + self.batch_size

        images1 = self.seq_images1[ini_index:final_index]
        images2 = self.seq_images2[ini_index:final_index]
        images3 = self.seq_images3[ini_index:final_index]
        states = self.seq_states[ini_index:final_index]
        statesnoa = self.seq_statesnoa[ini_index:final_index]
        descriptions = self.seq_descriptions[ini_index:final_index]

        self.ipos = final_index

        return images1,images2,images3,states,statesnoa,descriptions,self.complete_flag

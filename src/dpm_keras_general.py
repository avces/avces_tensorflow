#
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import numpy as np
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import random_ops
from tensorflow.python.ops import array_ops


def count_trainable_parameters():
    total_parameters = 0
    print("\n displaying all trainable variables:\n")
    for variable in tf.trainable_variables():
        # shape is an array of tf.Dimension
        shape = variable.get_shape()

        variable_parametes = 1
        for dim in shape:
            variable_parametes *= dim.value
        print(variable.name, "   ", variable_parametes)
        total_parameters += variable_parametes
    print("TOTAL TRAINABLE PARAMETERS", total_parameters)
    print("\n")

def dropout_noscale(x, keep_prob):
    """ MC dropout:  applied at test time; no scaling for non-dropped elements"""
    #  return x if the keep_prob is 1
    if keep_prob == 1.0:
        return x
    # generate random matrix (each element [0, 1]) of same size as x, add keep_prob
    random_tensor = keep_prob + random_ops.random_uniform(array_ops.shape(x), dtype=x.dtype)
    # apply floor function so that result is 0. if [keep_prob, 1.0) and 1. if [1.0, 1.0 + keep_prob)
    binary_tensor = math_ops.floor(random_tensor)
    # elementwise multiplication with x
    ret = x * binary_tensor
    #  reshape to original shape of x
    ret.set_shape(x.get_shape())
    #  return
    return ret

#
# function defining network/graph
def network(FLAGS, images1_in=None, images2_in=None, images3_in=None, states_in=None):
    # inputs:  e.g., 32-example batch of 5-frame sequences of 64x64x1 images  [32, 5, 64, 64, 1]
    #
    # placeholder for phase variable; phase=True for training, phase=False for feedforward (e.g., predictions from trained model)
    phase = tf.placeholder(tf.bool)
    if phase == True:
        mc_dropout_rate = float(FLAGS.mc_dropout_rate)
    else:
        mc_dropout_rate = 0.0

    if FLAGS.SFP_enable == True:
        keep_prob_mc = 1.0 - float(FLAGS.mc_dropout_rate)
        # apply MC dropout to inputs - no scaling
        if FLAGS.image_pipeline_mode > 0:
            images1_in = dropout_noscale(images1_in, keep_prob_mc)
        if FLAGS.image_pipeline_mode > 1:
            images2_in = dropout_noscale(images2_in, keep_prob_mc)
        if FLAGS.image_pipeline_mode > 2:
            images3_in = dropout_noscale(images3_in, keep_prob_mc)
        
        if FLAGS.isa_enable == True:        
            states_in = dropout_noscale(states_in, keep_prob_mc)

    # param sizes
    kernel_size = [5, 5]
    num_features = 8
    rows1 = FLAGS.image_rows
    cols1 = FLAGS.image_cols
    stride1 = 1
    rows2 = int(rows1/stride1)
    cols2 = int(cols1/stride1)
    stride2 = 2
    rows3 = int(rows2/stride2)
    cols3 = int(cols2/stride2)
    stride3 = 2
    rows_out = int(rows3/stride3)
    cols_out = int(cols3/stride3)

    merge_one = []

    if FLAGS.image_pipeline_mode > 0:

        if images1_in != None:
            # BRANCH1.1
            # conv1
            conv11_out = tf.keras.layers.ConvLSTM2D(filters=num_features, kernel_size=kernel_size, strides=stride1, padding="same", \
                                activation="hard_sigmoid", recurrent_activation="tanh", \
                                kernel_initializer="glorot_uniform", recurrent_initializer="orthogonal", \
                                bias_initializer="zeros", unit_forget_bias=True, return_sequences=True, \
                                dropout=mc_dropout_rate, recurrent_dropout=mc_dropout_rate \
                                )(images1_in)

            # conv2
            conv12_out = tf.keras.layers.ConvLSTM2D(filters=num_features, kernel_size=kernel_size, strides=stride2, padding="same", \
                                activation="hard_sigmoid", recurrent_activation="tanh", \
                                kernel_initializer="glorot_uniform", recurrent_initializer="orthogonal", \
                                bias_initializer="zeros", unit_forget_bias=True, return_sequences=True, \
                                dropout=mc_dropout_rate, recurrent_dropout=mc_dropout_rate \
                                )(conv11_out)

            # conv3
            conv13_out = tf.keras.layers.ConvLSTM2D(filters=num_features, kernel_size=kernel_size, strides=stride3, padding="same", \
                                activation="hard_sigmoid", recurrent_activation="tanh", \
                                kernel_initializer="glorot_uniform", recurrent_initializer="orthogonal", \
                                bias_initializer="zeros", unit_forget_bias=True, return_sequences=True, \
                                dropout=mc_dropout_rate, recurrent_dropout=mc_dropout_rate \
                                )(conv12_out)

            # flat1
            flat11 = tf.reshape(conv13_out, [-1, FLAGS.seq_length * rows_out * cols_out * num_features])

            merge_one = tf.concat([flat11], 1)

        else:
            print("entered image pipeline mode > 0 but no input for images1 pipeline, skipping images1_in\n");

    if FLAGS.image_pipeline_mode > 1: 

        if images2_in != None:
            # BRANCH1.2
            # conv1
            conv21_out = tf.keras.layers.ConvLSTM2D(filters=num_features, kernel_size=kernel_size, strides=stride1, padding="same", \
                                activation="hard_sigmoid", recurrent_activation="tanh", \
                                kernel_initializer="glorot_uniform", recurrent_initializer="orthogonal", \
                                bias_initializer="zeros", unit_forget_bias=True, return_sequences=True, \
                                dropout=mc_dropout_rate, recurrent_dropout=mc_dropout_rate \
                                )(images2_in)

            # conv2
            conv22_out = tf.keras.layers.ConvLSTM2D(filters=num_features, kernel_size=kernel_size, strides=stride2, padding="same", \
                                activation="hard_sigmoid", recurrent_activation="tanh", \
                                kernel_initializer="glorot_uniform", recurrent_initializer="orthogonal", \
                                bias_initializer="zeros", unit_forget_bias=True, return_sequences=True, \
                                dropout=mc_dropout_rate, recurrent_dropout=mc_dropout_rate \
                                )(conv21_out)

            # conv3
            conv23_out = tf.keras.layers.ConvLSTM2D(filters=num_features, kernel_size=kernel_size, strides=stride3, padding="same", \
                                activation="hard_sigmoid", recurrent_activation="tanh", \
                                kernel_initializer="glorot_uniform", recurrent_initializer="orthogonal", \
                                bias_initializer="zeros", unit_forget_bias=True, return_sequences=True, \
                                dropout=mc_dropout_rate, recurrent_dropout=mc_dropout_rate \
                                )(conv22_out)

            # flat1
            flat21 = tf.reshape(conv23_out, [-1, FLAGS.seq_length * rows_out * cols_out * num_features])

            #merging the second image pipeline
            merge_one = tf.concat([merge_one, flat21], 1)

        else:
            print("entered image pipeline mode > 1 but no input for images2 pipeline, skipping images2_in\n");

    if FLAGS.image_pipeline_mode > 2: 

        if images3_in != None:
            # BRANCH1.3
            # conv1
            conv31_out = tf.keras.layers.ConvLSTM2D(filters=num_features, kernel_size=kernel_size, strides=stride1, padding="same", \
                                activation="hard_sigmoid", recurrent_activation="tanh", \
                                kernel_initializer="glorot_uniform", recurrent_initializer="orthogonal", \
                                bias_initializer="zeros", unit_forget_bias=True, return_sequences=True, \
                                dropout=mc_dropout_rate, recurrent_dropout=mc_dropout_rate \
                                )(images3_in)

            # conv2
            conv32_out = tf.keras.layers.ConvLSTM2D(filters=num_features, kernel_size=kernel_size, strides=stride2, padding="same", \
                                activation="hard_sigmoid", recurrent_activation="tanh", \
                                kernel_initializer="glorot_uniform", recurrent_initializer="orthogonal", \
                                bias_initializer="zeros", unit_forget_bias=True, return_sequences=True, \
                                dropout=mc_dropout_rate, recurrent_dropout=mc_dropout_rate \
                                )(conv31_out)

            # conv3
            conv33_out = tf.keras.layers.ConvLSTM2D(filters=num_features, kernel_size=kernel_size, strides=stride3, padding="same", \
                                activation="hard_sigmoid", recurrent_activation="tanh", \
                                kernel_initializer="glorot_uniform", recurrent_initializer="orthogonal", \
                                bias_initializer="zeros", unit_forget_bias=True, return_sequences=True, \
                                dropout=mc_dropout_rate, recurrent_dropout=mc_dropout_rate \
                                )(conv32_out)

            # flat1
            flat31 = tf.reshape(conv33_out, [-1, FLAGS.seq_length * rows_out * cols_out * num_features])

            #merging the third image pipeline
            merge_one = tf.concat([merge_one, flat31], 1)

        else:
            print("entered image pipeline mode > 2 but no input for images3 pipeline, skipping images3_in\n");

    if FLAGS.isa_enable == True:
        #
        # BRANCH2
        # LSTM1

        if states_in != None:
            lstm1_out = tf.keras.layers.LSTM(units=FLAGS.num_states, activation="hard_sigmoid", \
                                recurrent_activation="tanh", use_bias=True, \
                                kernel_initializer="glorot_uniform", recurrent_initializer="orthogonal", \
                                bias_initializer="zeros", unit_forget_bias=True, return_sequences=True, \
                                dropout=mc_dropout_rate, recurrent_dropout=mc_dropout_rate \
                                )(states_in)

            # flat2
            flat2 = tf.reshape(lstm1_out, [-1, FLAGS.seq_length * FLAGS.num_states])

            # merge BRANCH1 and BRANCH2
            merge_one = tf.concat([merge_one, flat2], 1)
        else:
            print("isa_enbale is true but states_in is None, skipping the state and action pipeline\n")
    #

    # dense1
    W_d1 = weight_variable([merge_one.get_shape().as_list()[1], 64])
    b_d1 = bias_variable([64], init_value=0.0)
    dense1 = tf.nn.relu(tf.matmul(merge_one, W_d1) + b_d1)

    # apply dense_dropout to fully-connected dense layer
    if phase == True:
        keep_prob = 1.0 - FLAGS.dense_drop
    else:
        keep_prob = 1.0

    if FLAGS.SFP_enable == True:
        keep_prob = 1.0 - float(FLAGS.dense_drop)
        dense1 = dropout_noscale(dense1, keep_prob)
    else:
        dense1 = tf.nn.dropout(dense1, keep_prob)
    #
    # dense2
    W_d2 = weight_variable([64, FLAGS.nb_classes])
    b_d2 = bias_variable([FLAGS.nb_classes], init_value=0.0)
    dense2 = tf.nn.softmax(tf.matmul(dense1, W_d2) + b_d2)

    # output
    output = dense2

    # count parameters
    count_trainable_parameters()

    # return output and placeholders created in this function

    if FLAGS.vis_enable == True:
        if FLAGS.image_pipeline_mode == 1:
            return output, phase, [conv11_out, conv12_out, conv13_out]
        elif FLAGS.image_pipeline_mode == 2:
            return output, phase, [conv11_out, conv12_out, conv13_out, conv21_out, conv22_out, conv23_out]
        elif FLAGS.image_pipeline_mode == 3:
            return output, phase, [conv11_out, conv12_out, conv13_out, conv21_out, conv22_out, conv23_out, conv31_out, conv32_out, conv33_out]
    else:
        return output, phase

def weight_variable(shape):
    """weight_variable generates a weight variable of a given shape."""
    #
    # xavier/glorot uniform initialization
    fan_in = shape[0]
    fan_out = shape[1]
    low = -1*np.sqrt(6.0/(fan_in + fan_out)) # use 4 for sigmoid, 1 for tanh activation (keras uses 1)
    high = 1*np.sqrt(6.0/(fan_in + fan_out))
    initial = tf.random_uniform(shape, minval=low, maxval=high, dtype=tf.float32)
    return tf.Variable(initial)

def bias_variable(shape, init_value=0.1):
    """bias_variable generates a bias variable of a given shape."""
    initial = tf.constant(init_value, shape=shape)
    return tf.Variable(initial)
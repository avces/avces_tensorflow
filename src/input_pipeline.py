#
# 8/19/18
import tensorflow as tf
import skimage.io as io
from os import listdir
from os.path import isfile, join

class DataGen():

    def __init__(self, FLAGS, mode = 'training'):
        self.mode = mode
        self.FLAGS = FLAGS
        self.get_dataset()


    def _read_and_decode(self, serialized_example):

        if self.FLAGS.num_states != 0:

            features = tf.parse_single_example(
              serialized_example,
              features={
                'num_examples': tf.FixedLenFeature([], tf.int64), 
                'seq_length': tf.FixedLenFeature([], tf.int64),
                'rows': tf.FixedLenFeature([], tf.int64),
                'cols': tf.FixedLenFeature([], tf.int64),
                'channels': tf.FixedLenFeature([], tf.int64),
                'label1_raw': tf.FixedLenFeature([], tf.string),
                'label2_raw': tf.FixedLenFeature([], tf.string),
                'label3_raw': tf.FixedLenFeature([], tf.string),
                'label4_raw': tf.FixedLenFeature([], tf.string),
                'label5_raw': tf.FixedLenFeature([], tf.string),
                'image1_raw': tf.FixedLenFeature([], tf.string),
                'image2_raw': tf.FixedLenFeature([], tf.string),
                'image3_raw': tf.FixedLenFeature([], tf.string),
                'stateinfo_raw': tf.FixedLenFeature([], tf.string),
                'statenoa_raw': tf.FixedLenFeature([], tf.string),
                })

        else:
            features = tf.parse_single_example(
              serialized_example,
              features={
                'num_examples': tf.FixedLenFeature([], tf.int64),
                'seq_length': tf.FixedLenFeature([], tf.int64),
                'rows': tf.FixedLenFeature([], tf.int64),
                'cols': tf.FixedLenFeature([], tf.int64),
                'channels': tf.FixedLenFeature([], tf.int64),
                'label1_raw': tf.FixedLenFeature([], tf.string),
                'label2_raw': tf.FixedLenFeature([], tf.string),
                'label3_raw': tf.FixedLenFeature([], tf.string),
                'label4_raw': tf.FixedLenFeature([], tf.string),
                'label5_raw': tf.FixedLenFeature([], tf.string),
                'image1_raw': tf.FixedLenFeature([], tf.string),
                'image2_raw': tf.FixedLenFeature([], tf.string),
                'image3_raw': tf.FixedLenFeature([], tf.string),
                })

        # Convert from a scalar string tensor 
        image1 = tf.decode_raw(features['image1_raw'], tf.float32)
        image2 = tf.decode_raw(features['image2_raw'], tf.float32)
        image3 = tf.decode_raw(features['image3_raw'], tf.float32)
        label1= tf.decode_raw(features['label1_raw'], tf.int64)
        label2= tf.decode_raw(features['label2_raw'], tf.int64)
        label3= tf.decode_raw(features['label3_raw'], tf.int64)
        label4= tf.decode_raw(features['label4_raw'], tf.int64)
        label5= tf.decode_raw(features['label5_raw'], tf.int64)

        label1 = tf.cast(tf.cast(label1,tf.bool), tf.int8)
        label2 = tf.cast(tf.cast(label2,tf.bool), tf.int8)
        label3 = tf.cast(tf.cast(label3,tf.bool), tf.int8)
        label4 = tf.cast(tf.cast(label4,tf.bool), tf.int8)
        label5 = tf.cast(tf.cast(label5,tf.bool), tf.int8)

        self.rows = tf.cast(features['rows'], tf.int32)
        self.cols = tf.cast(features['cols'], tf.int32)
        self.channels = tf.cast(features['channels'], tf.int32)
        self.seq_length = tf.cast(features['seq_length'], tf.int32)
        
        self.movie_shape = tf.constant([self.FLAGS.seq_length, self.FLAGS.image_rows, self.FLAGS.image_cols, self.FLAGS.image_chans])

        self.label_shape = tf.constant([1,self.FLAGS.nb_classes])

        movie1 = tf.reshape(image1, self.movie_shape)
        movie2 = tf.reshape(image2, self.movie_shape)
        movie3 = tf.reshape(image3, self.movie_shape)
        label1 = tf.reshape(label1, self.label_shape)
        label2 = tf.reshape(label2, self.label_shape)
        label3 = tf.reshape(label3, self.label_shape)
        label4 = tf.reshape(label4, self.label_shape)
        label5 = tf.reshape(label5, self.label_shape)

        features_out = tf.stack([movie1, movie2, movie3])
        labels_out = tf.stack([label1, label2, label3, label4, label5])

        if self.FLAGS.num_states != 0:
            stateinfo = tf.decode_raw(features['stateinfo_raw'], tf.float64)
            statenoa = tf.decode_raw(features['statenoa_raw'], tf.float64)
            
            stateinfo = tf.cast(stateinfo, tf.float32)
            statenoa = tf.cast(statenoa, tf.float32)
            
            self.shape_stateinfo = tf.constant([self.FLAGS.seq_length, self.FLAGS.num_states])
            self.shape_statenoa = tf.constant([self.FLAGS.seq_length, self.FLAGS.num_states_noa])

            stateinfo = tf.reshape(stateinfo, self.shape_stateinfo, name='state_rs')
            statenoa = tf.reshape(statenoa, self.shape_statenoa, name='statenoa_rs')

            return features_out,labels_out,stateinfo,statenoa

        else:
            return features_out,labels_out

    def get_dataset(self):

        if self.mode == 'training':
            files = tf.data.Dataset.list_files(join(self.FLAGS.tfrecords_dir, "train", "*.tfrecords"))
        elif self.mode == 'validate':
            files = tf.data.Dataset.list_files(join(self.FLAGS.tfrecords_dir, "validate", "*.tfrecords"))
        else:
            files = tf.data.Dataset.list_files(join(self.FLAGS.tfrecords_dir, "test", "*.tfrecords"))

        dataset = files.interleave(tf.data.TFRecordDataset, cycle_length = 2)
        dataset = dataset.apply(tf.contrib.data.shuffle_and_repeat(buffer_size = 2*self.FLAGS.batch_size))
        dataset = dataset.map(map_func=self._read_and_decode, num_parallel_calls=self.FLAGS.num_parallel_calls)

        dataset = dataset.batch(batch_size=self.FLAGS.batch_size)
        dataset = dataset.prefetch(buffer_size=2*self.FLAGS.batch_size)

        self.dataset = dataset

import numpy as np
import os
import cv2
import sys

parser = argparse.ArgumentParser(description = "parser to parse arguments to makevideo script")

root_path = os.path.normpath(os.getcwd() + os.sep + os.pardir)

parser.add_argument('--img_path', type=str, default=os.path.join(root_path, "infdata", "images"),
                     help="folder path with the original inference images")
parser.add_argument('--img_save_path', type=str, default=os.path.join(root_path, "infdata", "images_marked"),
                     help="folder path to save the labeled frames")
parser.add_argument('--run_name', type=str, default=' ',
                     help='''run name or first part of {}_pred_labels.npy and {}_corres_descriptions.npy filenames
					 		Ex: if output filenames are 50-25-99-8_pred_labels.npy then --run would be '50-25-99-8'
							Refer readme for simulation data naming convention extracted from webots ''')
parser.add_argument('--video_save_location', type=str, default=os.getcwd(),
                     help="location to save the video")
parser.add_argument('--video_file_name', type=str, default="video_test.avi",
                     help="name of of the video file created in .avi format")
parser.add_argument('--width', type=int, default=256,
                     help="width of video (one parameter of resolution)")
parser.add_argument('--height', type=int, default=256,
                     help="height of video (one parameter of resolution)")


args = parser.parse_args()

sys.path.append(args.img_path)

img_list = os.listdir(args.img_path)
video = cv2.VideoWriter(os.path.join(args.video_save_location, args.video_file_name),-1,20,(args.width,args.height),1)
frame_nums = []

for f in img_list:
	fexec = f.split('.')
	fparts = fexec[0].split('-')
	frame_nums.append(int(fparts[-2]))

sort = np.argsort(np.array(frame_nums))
img_list = np.array(img_list)
img_list = img_list[sort]

for f in img_list:
	fexec = f.split('.')
	fparts = fexec[0].split('-')
	run_num = fparts[0] + '-' + fparts[1] + '-' + fparts[2] + '-' + fparts[3]

	if run_num != args.run_name:
		continue
	cam = int(float(fparts[-1]))

	if cam != 1:
		continue
	print(f)
	img = cv2.imread(os.path.join(args.img_save_path, f))
	print(img.shape)
	video.write(img)

cv2.destroyAllWindows()
video.release()

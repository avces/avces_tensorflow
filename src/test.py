#
import os
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID" # so the IDs match nvidia-smi
os.environ["CUDA_VISIBLE_DEVICES"] = "0,1" # "0, 1" for multiple gpus

# import network from dpm_* file
import dpm_keras_general as dpm
import tensorflow as tf

import numpy as np
import os
import time
from sklearn import metrics as skmetrics
import flags_initialize
import input_pipeline
import input_pipeline_np
import datetime
import sys
import scipy
#
#
def test(FLAGS, num_test):
    """Train the model."""
    #
    # start the clock
    start_time = time.time()

    acc_train = []
    MCC_train = []
    # reset the default graph
    tf.keras.backend.clear_session()
    tf.reset_default_graph()

    if FLAGS.inference == False:
        #creating and fetching tf.data.Dataset object for test data
        test_dataset = input_pipeline.DataGen(mode='test',FLAGS=FLAGS)

        #creating and initializing iterators for the dataset objects
        test_iterator = tf.data.Iterator.from_structure(test_dataset.dataset.output_types,test_dataset.dataset.output_shapes)

        test_init_op = test_iterator.make_initializer(test_dataset.dataset)

        next_test_batch = test_iterator.get_next()
    else:
        test_dataset = input_pipeline_np.getData(FLAGS)

    # Create the placeholder for the model inputs

    x_images1 = tf.placeholder(tf.float32, [None, FLAGS.seq_length, FLAGS.image_rows, FLAGS.image_cols, FLAGS.image_chans])
    x_images2 = tf.placeholder(tf.float32, [None, FLAGS.seq_length, FLAGS.image_rows, FLAGS.image_cols, FLAGS.image_chans])
    x_images3 = tf.placeholder(tf.float32, [None, FLAGS.seq_length, FLAGS.image_rows, FLAGS.image_cols, FLAGS.image_chans])
    x_sa = tf.placeholder(tf.float32, [None, FLAGS.seq_length, FLAGS.num_states])

    if FLAGS.inference == False:
            # Create placeholder for ground truth labels
        y_ = tf.placeholder(tf.int8, [None, FLAGS.nb_classes])


    # Build the graph for the deep network
    if FLAGS.vis_enable == False:
        y_conv, phase = dpm.network(FLAGS, x_images1, x_images2, x_images3, x_sa)
    else:
        y_conv, phase, vis_ops = dpm.network(FLAGS, x_images1, x_images2, x_images3, x_sa)

    inference_output = tf.argmax(y_conv,1)

    if FLAGS.inference == False:
        with tf.variable_scope("accuracy_calc"):
            correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    saver = tf.train.Saver()
    # init vbls
    init_op = tf.group(tf.global_variables_initializer(),
                       tf.local_variables_initializer())

    config = tf.ConfigProto(log_device_placement=FLAGS.LOG_DEVICE_PLACEMENT)
    config.gpu_options.allow_growth = True
    #
    with tf.Session(config=config) as sess:
        sess.run(init_op)

        print("restoring checkpoint file",FLAGS.restore_model_file)
        saver.restore(sess, FLAGS.restore_model_file)

        pred_labels = np.array([], dtype=np.int8)

        if FLAGS.inference == False:
            sess.run(test_init_op)
            ground_truth = np.array([], dtype=np.int8)
            temp_test_acc = []

            for j in range(int(num_test/FLAGS.batch_size)):
                features,labels,stateinfo,statenoa = sess.run(next_test_batch)

                test_accuracy,inf = sess.run([accuracy,inference_output],feed_dict={x_images1: features[:,0], x_images2: features[:,1],
                x_images3: features[:,2], y_: np.squeeze(labels[:,FLAGS.labels_sec-1]), x_sa: stateinfo, phase: False})

                pred_labels = scipy.sparse.hstack([pred_labels,inf.astype(np.int8)]).toarray()
                ground_truth = scipy.sparse.hstack([ground_truth,np.argmax(np.squeeze(labels[:,FLAGS.labels_sec-1]),1).astype(np.int8)]).toarray()
                temp_test_acc.append(test_accuracy)

            print("pred_labels:",pred_labels)
            print("ground_truth", ground_truth)

            #adding validation accuracies per epoch to list
            print("testing accuracies for individual batches: ", temp_test_acc)
            print("mean test accuracy: ", np.mean(temp_test_acc))

        else:

            corres_descriptions = np.array([], dtype=str)

            while(1):
                images1,images2,images3,stateinfo,statenoa,descriptions,complete_flag = test_dataset.get_next_batch()

                inf = sess.run([inference_output],feed_dict={x_images1: images1, x_images2: images2,
                x_images3: images3, x_sa: stateinfo, phase: False})

                if len(pred_labels) == 0:
                    pred_labels = inf
                else:
                    pred_labels = np.concatenate([pred_labels, inf], axis = 1)

                if corres_descriptions.size == 0:
                    corres_descriptions = descriptions[:,4]
                else:
                    corres_descriptions = np.concatenate([corres_descriptions,descriptions[:,4]])

                if complete_flag == 1:
                    break

            basename = os.path.basename(FLAGS.inf_npzfile)
            run_name = basename.split('.')[0]
            output_save_path = os.path.join(FLAGS.infdata_dir, "inf_output")
            if os.path.isdir(output_save_path) == False:
                os.mkdir(output_save_path)
            print("{}_pred_labels: ".format(run_name), pred_labels)
            print("corres_descriptions: ", corres_descriptions)
            np.save(os.path.join(output_save_path, "{}_pred_labels".format(run_name)), pred_labels)
            np.save(os.path.join(output_save_path, "{}_corres_descriptions".format(run_name)), corres_descriptions)

    # check the clock
    elap_time = time.time() - start_time
    print('Elapsed time to train and test : %.2f seconds' % (elap_time))
    #

def main(argv=None):
    now = datetime.datetime.now
    print(now)
    FLAGS = flags_initialize.define_flags()

    print("entering test phase....\n")

    tfrecord_details_path = os.path.join(FLAGS.tfrecords_dir, FLAGS.tfrecord_details_file)
    if os.path.isfile(tfrecord_details_path):
        with open(tfrecord_details_path) as file:
            lines = file.read().splitlines()
            for line in lines:
                words = line.split(' ')
                if words[0] == 'training':
                    num_train = int(float(words[1]))
                elif words[0] == 'validation':
                    num_validate = int(float(words[1]))
                else:
                    num_test = int(float(words[1]))

        print("number of training samples: %d" % num_train)
        print("number of validation samples: %d" % num_validate)

    tfrecord_coll_info_path = os.path.join(FLAGS.tfrecords_dir, FLAGS.tfrecord_coll_info)
    if os.path.isfile(tfrecord_coll_info_path):
        with open(tfrecord_coll_info_path) as file:
            lines = file.read().splitlines()
            for line in lines:
                print(line)

    print("batch size: %d" % FLAGS.batch_size)
    print("number of classes: %d" % FLAGS.nb_classes)
    print("number of proprioceptive states + actions: %d" % FLAGS.num_states)
    print("number of proprioceptive states without actions: %d" % FLAGS.num_states_noa)
    print("image dimensions (width,height,channels): (%d,%d,%d)" % (FLAGS.image_rows, FLAGS.image_cols,FLAGS.image_chans))
    print("sequence length or number of frames for one inference: %d" % FLAGS.seq_length)
    print("image_pipeline mode or number of parallel sequence channels to the network: %d" % FLAGS.image_pipeline_mode)
    print("isa_enable or FLAG for enabling or disabling the state+actions channel in the input to network: %s" % FLAGS.isa_enable)
    print("SFP_enbale flag to enale or disable stochastic forward passes in the network: %s" % FLAGS.SFP_enable)

    test(FLAGS, num_test)

if __name__ == '__main__':
    tf.app.run()

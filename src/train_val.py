#
#rev 8/19/18
import os
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID" # so the IDs match nvidia-smi
os.environ["CUDA_VISIBLE_DEVICES"] = "0,1" # "0, 1" for multiple gpus

# import network from dpm_* file
import dpm_keras_general as dpm
import tensorflow as tf
from tensorflow.python.client import timeline

# old import stmts
import numpy as np
import os
import time
from sklearn import metrics as skmetrics
from tqdm import tqdm
from sklearn.utils import shuffle
import flags_initialize
import input_pipeline
import datetime
import shutil
#
#
def train(FLAGS, num_train, num_validate):
    """Train the model."""
    #
    # start the clock
    start_time = time.time()
    #
    acc_train = []
    MCC_train = []
    # reset the default graph
    tf.keras.backend.clear_session()
    tf.reset_default_graph()

    #creating and fetching tf.data.Dataset object for training and validation data
    train_dataset = input_pipeline.DataGen(mode='training',FLAGS=FLAGS)
    valid_dataset = input_pipeline.DataGen(mode='validate', FLAGS=FLAGS)

    #creating and initializing iterators for the dataset objects
    train_iterator = tf.data.Iterator.from_structure(train_dataset.dataset.output_types,train_dataset.dataset.output_shapes)
    valid_iterator = tf.data.Iterator.from_structure(valid_dataset.dataset.output_types,valid_dataset.dataset.output_shapes)

    training_init_op = train_iterator.make_initializer(train_dataset.dataset)
    valid_init_op = valid_iterator.make_initializer(valid_dataset.dataset)

    next_training_batch = train_iterator.get_next()
    next_validate_batch = valid_iterator.get_next()

    # Create the placeholder for the model inputs

    x_images1 = tf.placeholder(tf.float32, [None, FLAGS.seq_length, FLAGS.image_rows, FLAGS.image_cols, FLAGS.image_chans])
    x_images2 = tf.placeholder(tf.float32, [None, FLAGS.seq_length, FLAGS.image_rows, FLAGS.image_cols, FLAGS.image_chans])
    x_images3 = tf.placeholder(tf.float32, [None, FLAGS.seq_length, FLAGS.image_rows, FLAGS.image_cols, FLAGS.image_chans])
    x_sa = tf.placeholder(tf.float32, [None, FLAGS.seq_length, FLAGS.num_states])

    # Create placeholder for ground truth labels
    y_ = tf.placeholder(tf.int8, [None, FLAGS.nb_classes])

    # variable to keep track of global step
    global_step = tf.Variable(0,name="global_step", trainable=False)

    # Build the graph for the deep network
    if FLAGS.vis_enable == False:
         y_conv, phase = dpm.network(FLAGS, x_images1, x_images2, x_images3, x_sa)
    else:
         y_conv, phase, vis_ops = dpm.network(FLAGS, x_images1, x_images2, x_images3, x_sa)

    with tf.variable_scope("cross_entropy_loss"):
        cross_entropy = tf.reduce_mean(
          tf.nn.softmax_cross_entropy_with_logits_v2(labels=y_, logits=y_conv))

    with tf.variable_scope("train/grads_and_opt"):
        opt = tf.train.AdamOptimizer(FLAGS.lr, name="opt")#.minimize(cross_entropy)

        train_vars = tf.trainable_variables()
        grads = opt.compute_gradients(cross_entropy, var_list = train_vars)
        apply_grads = opt.apply_gradients(grads, name = "grad_apply_op", global_step=global_step)

    with tf.variable_scope("accuracy_calc"):
        correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    train_step = apply_grads

    ######Summary ops and histograms##########
    # histograms of parameters
    for variable in tf.trainable_variables():
        variable_name = variable.name
        tf.summary.histogram(variable.name, variable)

    #histograms of gradients
    for grad, var in grads:
        if grad is not None:
            tf.summary.histogram(var.op.name + '/gradient', grad)

    tf.summary.scalar('accuracy', accuracy)
    tf.summary.scalar('cross_entropy_loss', cross_entropy)

    # merge all the summaries
    merged_summary = tf.summary.merge_all()
    # define a filewriter to write the summaries
    writer = tf.summary.FileWriter(os.path.join(FLAGS.filewriter_dir ,"run{}".format(str(FLAGS.run_num))))
    # Create a saver to save checkpoints
    if args.epochs_per_chkpt < 0:
        saver = tf.train.Saver(max_to_keep=1)
    else:
        saver = tf.train.Saver(max_to_keep=args.max_to_keep)

    max_train_accuracy = 0.0
    prev_train_acc_epoch = 0.0

    # init vbls
    init_op = tf.group(tf.global_variables_initializer(),
                       tf.local_variables_initializer())

    config = tf.ConfigProto(log_device_placement=FLAGS.LOG_DEVICE_PLACEMENT)
    config.gpu_options.allow_growth = True
    #
    with tf.Session(config=config) as sess:
        sess.run(init_op)
        #adding graph to tensorboard
        writer.add_graph(sess.graph)

        if FLAGS.restore_model == True:
            print("restoring model file ", FLAGS.restore_model_file)
            saver.restore(sess, FLAGS.restore_model_file)

        if FLAGS.timeline_enable == True:
            #enabling options for full trace used for time profiling
            options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            run_metadata = tf.RunMetadata()
        else:
            options = tf.RunOptions()
            run_metadata = tf.RunMetadata()

        sess.run(training_init_op)
        sess.run(valid_init_op)

        epochs_training_acc = []
        epochs_validate_acc = []
        #
        # training epochs
        for epoch in tqdm(range(FLAGS.num_epochs)):
            list_train_acc = []
            list_valid_acc = []
            # loop through the data, use 1 out of every 30 batches for verification
            batches_per_epoch = int(num_train/FLAGS.batch_size)
            for i in range(batches_per_epoch):
                # each iteration uses batch_size examples
                # verify traning and validation accuracies every steps_verif steps
                #
                if (i+1) % FLAGS.steps_verif == 0:
                    # get the next training batch
                    features,labels,stateinfo,statenoa = sess.run(next_training_batch)
                    # feed data and perform train iteration
                    train_accuracy = sess.run(accuracy,feed_dict={x_images1: features[:,0], x_images2: features[:,1],
                    x_images3: features[:,2], y_: np.squeeze(labels[:,FLAGS.labels_sec-1]), x_sa: stateinfo, phase: False},options=options, run_metadata=run_metadata)

                    print('epoch %d, step %d, training accuracy %.6f' % (epoch+args.base_epoch+1, i+1, train_accuracy))

                    list_train_acc.append(train_accuracy)

                    # only save if train_accuracy matched or exceeded previous max
                    if args.epochs_per_chkpt < 0:
                        if train_accuracy >= max_train_accuracy:
                            saver.save(sess, os.path.join(FLAGS.chkpt_dir , "run{}".format(str(FLAGS.run_num)), 'model_maxaccu_epoch_' + str(args.base_epoch+epoch+1) + '_step_' + str(i+1)) , global_step=global_step)
                            max_train_accuracy = train_accuracy

                    # perform and store validation accuracy on all validation data
                    temp_val_acc = []
                    for j in range(int(num_validate/FLAGS.batch_size)):

                        features,labels,stateinfo,statenoa = sess.run(next_validate_batch)

                        valid_accuracy,crct_pred = sess.run([accuracy,correct_prediction],feed_dict={x_images1: features[:,0], x_images2: features[:,1],
                        x_images3: features[:,2], y_: np.squeeze(labels[:,FLAGS.labels_sec-1]), x_sa: stateinfo, phase: False},options=options, run_metadata=run_metadata)
                        temp_val_acc.append(valid_accuracy)

                    print('train_epoch %d, step %d, mean validation accuracy %.6f' % (args.base_epoch+epoch, i, np.mean(temp_val_acc)))
                    list_valid_acc.append(np.mean(temp_val_acc))

                else:
                    features, labels,stateinfo,statenoa = sess.run(next_training_batch)

                    # perform the training step
                    #  x:         input
                    #  y_:        ground truth label
                    #  phase:     indicator for training (True for training, False otherwise)

                    sess.run(train_step,feed_dict={x_images1: features[:,0], x_images2: features[:,1],
                    x_images3: features[:,2], y_: np.squeeze(labels[:,FLAGS.labels_sec-1]), x_sa: stateinfo, phase: True}, options=options, run_metadata=run_metadata)

                if (i+1)%FLAGS.DISPLAY_STEP == 0:
                    s = sess.run(merged_summary, feed_dict={x_images1: features[:,0], x_images2: features[:,1],
                    x_images3: features[:,2], y_: np.squeeze(labels[:,FLAGS.labels_sec-1]), x_sa: stateinfo, phase: True})
                    writer.add_summary(s, (args.base_epoch+epoch)*batches_per_epoch + i)

                    if FLAGS.timeline_enable == True:
                        ### fetching and storing timeline stats ###
                        fetched_timeline = timeline.Timeline(run_metadata.step_stats)
                        chrome_trace = fetched_timeline.generate_chrome_trace_format()
                        with open(os.path.join(FLAGS.timeline_dir,"run{}".format(str(FLAGS.run_num)),'timeline_step%d_epoch%d.json' % (i,epoch+args.base_epoch)), 'w') as f:
                            f.write(chrome_trace)

            # end of for-loop for each epoch
            #
            train_acc_epoch = np.mean(list_train_acc)
            #adding training accuracies per epoch to list
            epochs_training_acc.append(train_acc_epoch)

            # stop if training accuracy decreases
            if train_acc_epoch < prev_train_acc_epoch:
                #  accuracy decreased
                print("mean of training accuracy over the epoch decreased; epoch num %d, step %d", %(epoch+args.base_epoch,i))

            # save the params for every 3 epochs
            if args.epochs_per_chkpt >= 0:
                if (epoch+1) % args.epochs_per_chkpt == 0:
                    saver.save(sess, os.path.join(FLAGS.chkpt_dir,"run{}".format(str(FLAGS.run_num)), 'model_epoch_' + str(args.base_epoch+epoch+1)))

            # in any event, save the accuracy for next epoch's check
            prev_train_acc_epoch = train_acc_epoch

            #adding validation accuracies per epoch to list
            epochs_validate_acc.append(np.mean(list_valid_acc))
        # end of for loop for training epochs

        # printing training and testing accuracy details
        print("training accuracies for individual epochs: ", epochs_training_acc)
        print("validation accuracies for individual epochs: ", epochs_validate_acc)
        #
        # check the clock
        elap_time = time.time() - start_time
        print('Elapsed time so far: %.2f seconds' % (elap_time))

    # stop the clock
    elap_time = time.time() - start_time
    print('Elapsed time to train and test : %.2f seconds' % (elap_time))
    #

def main(argv=None):
    # execute the train() function

    now = datetime.datetime.now
    print(now)
    FLAGS = flags_initialize.define_flags()
    print("entering training and validation phase....\n")
    print("displaying all the training parameters:\n")

    tfrecord_details_path = os.path.join(FLAGS.tfrecords_dir, FLAGS.tfrecord_details_file)
    if os.path.isfile(tfrecord_details_path):
        with open(tfrecord_details_path) as file:
            lines = file.read().splitlines()
            for line in lines:
                words = line.split(' ')
                if words[0] == 'training':
                    num_train = int(float(words[1]))
                elif words[0] == 'validation':
                    num_validate = int(float(words[1]))
                else:
                    num_test = int(float(words[1]))

        print("number of training samples: %d" % num_train)
        print("number of validation samples: %d" % num_validate)

    tfrecord_coll_info_path = os.path.join(FLAGS.tfrecords_dir, FLAGS.tfrecord_coll_info)
    if os.path.isfile(tfrecord_coll_info_path):
        with open(tfrecord_coll_info_path) as file:
            lines = file.read().splitlines()
            for line in lines:
                print(line)

    print("learning rate: %f" % FLAGS.lr)
    print("bacth size: %d" % FLAGS.batch_size)
    print("number of epochs: %d" % FLAGS.num_epochs)
    print("number of training steps before verification: %d" % FLAGS.steps_verif)
    print("DISPLAY STEP to initialize number of steps before each summary writing: %d\n" % FLAGS.DISPLAY_STEP)
    print("number of classes: %d" % FLAGS.nb_classes)
    print("number of proprioceptive states + actions: %d" % FLAGS.num_states)
    print("number of proprioceptive states without actions: %d" % FLAGS.num_states_noa)
    print("image dimensions (width,height,channels): (%d,%d,%d)" % (FLAGS.image_rows, FLAGS.image_cols,FLAGS.image_chans))
    print("sequence length or number of frames for one inference: %d" % FLAGS.seq_length)
    print("image_pipeline mode or number of parallel sequence channels to the network: %d" % FLAGS.image_pipeline_mode)
    print("isa_enable or FLAG for enabling or disabling the state+actions channel in the input to network: %s" % FLAGS.isa_enable)
    print("SFP_enbale flag to enale or disable stochastic forward passes in the network: %s" % FLAGS.SFP_enable)

    train(FLAGS, num_train, num_validate)

if __name__ == '__main__':
    tf.app.run()

import numpy as np
import cv2
import os
import sys

parser = argparse.ArgumentParser(description = "parser to parse arguments to markframes script")

root_path = os.path.normpath(os.getcwd() + os.sep + os.pardir)

parser.add_argument('--img_path', type=str, default=os.path.join(root_path, "infdata", "images"),
                     help="folder path with the original inference images")
parser.add_argument('--img_save_path', type=str, default=os.path.join(root_path, "infdata", "images_marked"),
                     help="folder path to save the labeled frames")
parser.add_argument('--run_name', type=str, default=' ',
                     help='''run name or first part of {}_pred_labels.npy and {}_corres_descriptions.npy filenames
					 		Ex: if output filenames are 50-25-99-8_pred_labels.npy then --run would be '50-25-99-8'
							Refer readme for simulation data naming convention extracted from webots ''')
parser.add_argument('--inf_output_path', type=str, default=os.path.join(root_path, "infdata", "inf_output"),
                     help=" the folder path which has the .npy inference output files stored")
parser.add_argument('--seq_length', type=int, default=5,
                     help=" number of frames in sequence used while training the network")

args = parser.parse_args()

sys.path.append(args.img_path)
if os.path.isdir(args.img_save_path) == False:
	os.mkdir(args.img_save_path)

pred_labels = np.squeeze(np.load(os.path.join(args.inf_output_path, "{}_pred_labels.npy".format(args.run_name)))).tolist()
corres_descriptions = np.load(os.path.join(args.inf_output_path,"{}_corres_descriptions.npy".format(args.run_name))).tolist()

img_list = os.listdir(args.img_path)
frame_order = []

for frame in corres_descriptions:
	fexec = frame.split('.')
	fparts = fexec[0].split('-')
	frame_order.append(fparts[-1])

frame_order = np.array(frame_order)
pred_labels = np.array(pred_labels)
sort = np.argsort(frame_order.astype(np.int8))

frame_order = frame_order[sort]
pred_labels = pred_labels[sort]

scale = .6
thickness = 2
edge_margin = 20
margin = 5

for file in img_list:
	fexec = file.split('.')
	fparts = fexec[0].split('-')
	run_num = fparts[0] + '-' + fparts[1] + '-' + fparts[2] + '-' + fparts[3]

	if run_num != args.run_name:
		continue
	img = cv2.imread(os.path.join(args.img_path,file))

	frame_num = int(float(fexec[0].split('-')[-2]))
	if frame_num < args.seq_length - 1:
		cv2.imwrite(os.path.join(args.img_save_path, file), img)
		continue

	label = pred_labels[frame_num - args.seq_length - 1]

	text1 = "NoCollision"
	text2 = "Collision"

	retval= cv2.getTextSize(text1, cv2.FONT_HERSHEY_SIMPLEX, scale,thickness)

	txtx = img.shape[0] - retval[0][0] - edge_margin
	txty = retval[0][1] + edge_margin

	pt1x = txtx - margin
	pt1y = txty - margin - retval[0][1]
	pt2x = pt1x + 2*margin + retval[0][0]
	pt2y = pt1y + 2*margin + retval[0][1]

	if label == 1:
		cv2.rectangle(img, (int(pt1x), int(pt1y)), (int(pt2x), int(pt2y)), (0,0,255), cv2.FILLED )
		cv2.putText(img, text2, (int(txtx), int(txty)), cv2.FONT_HERSHEY_SIMPLEX, scale, (255,255,255), thickness , 2)
	else:
		cv2.rectangle(img, (int(pt1x), int(pt1y)), (int(pt2x), int(pt2y)), (0,255,0), cv2.FILLED )
		cv2.putText(img, text1, (int(txtx), int(txty)), cv2.FONT_HERSHEY_SIMPLEX, scale, (0,0,0), thickness , 2)

	cv2.imwrite(os.path.join(args.img_save_path, file), img)

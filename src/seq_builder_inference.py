#file for building sequences form RAW webots image data
import os
import time
from PIL import Image
import numpy as np
import csv
import argparse

# New process_image - uses Pillow and array
def process_image(imagepath):
    # Given an image, process it and return the array.
    #
    # load the image from the file path
    img = Image.open(imagepath)

    # convert to grayscale
    img_gray = img.convert('L')
    img_resize = img_gray.resize((256,256), Image.LANCZOS)

    # Turn it into numpy, normalize and return.
    img_array = np.array(img_resize)
    x = (img_array / 255.).astype(np.float32)
    return x

def get_extracted_image(filepath):
    """ Get the saved image, directly from filepath """
    if os.path.isfile(filepath):
        img1 = process_image(filepath)
    else:
        print("Error: get_extracted_image: file not found for filepath: ", filepath)
        exit()
    #
    return img1

def get_images_in_list(imagedir, list_files):
    list_images = []
    for filepath in list_files:
        image = get_extracted_image(os.path.join(imagedir, filepath))
        list_images.append(image)
    #
    return list_images

#list_imagefiles has list of images of cam1 sorted by frame number
def get_states_in_imagefile_list(statedir, list_imagefiles):
    # initialize the longer lists for all timesteps
    list_states = []
    list_statesnoa = []
    #
    for filepath in list_imagefiles:
        # get the first three parts of the filename
        fparts = filepath.split('-')
        statefileroot = fparts[0] + '-' + fparts[1] + '-' + fparts[2] + '-' + fparts[3] + '-' + fparts[4]
        # assemble the whole path to the state file
        statefilepath = os.path.join(statedir, statefileroot + '.state.txt')
        # extract the data from the state file
        state_info = np.genfromtxt(statefilepath, delimiter=" ", dtype=None)
        # convert the data to list format
        list_states_one_timestep = state_info.tolist()
        # add the data to the longer list for all timesteps
        list_states.append(list_states_one_timestep)
        # statesnoa
        # extract statenoa - remove second to last of 17 states
        list_statesnoa_one_timestep = list_states_one_timestep[0:-2]
        list_statesnoa_one_timestep.append(list_states_one_timestep[-1])
        # debug
        # print("length of list_statesnoa_one_timestep is: ", len(list_statesnoa_one_timestep))
        list_statesnoa.append(list_statesnoa_one_timestep)

    #  end of for loop for looping thru list_imagefiles
    return list_states, list_statesnoa

def put_images_into_sequences(list_images, seq_length):
    # initialize final array to return
    X1 = []
    # calculate the last image index to use as an initial image in the sequence
    last_index = len(list_images) - seq_length + 1
    for img_idx in range(0, last_index):
        # re-init the sequence, so the current sequence only include seq_length images
        sequence = []
        for spos in range(0, seq_length):
            img_num = img_idx + spos
            image = list_images[img_num]
            sequence.append(image)
        # add the current sequence to the final array
        X1.append(sequence)
    # return the final array
    return np.array(X1)

def put_states_into_sequences(list_states, list_statesnoa, seq_length):
    # initialize final arrays to return
    State = []
    Statenoa = []
    # calculate the last image index to use as an initial image in the sequence
    last_index = len(list_states) - seq_length + 1
    for st_idx in range(0, last_index):
        # re-init the sequence, so the current sequence only include seq_length states
        sequence_states = []
        sequence_statesnoa = []
        # iterate through each position in the sequence
        for spos in range(0, seq_length):
            st_num = st_idx + spos
            state = list_states[st_num]
            statenoa = list_statesnoa[st_num]
            sequence_states.append(state)
            sequence_statesnoa.append(statenoa)
        # add the current sequence to the final array
        State.append(sequence_states)
        Statenoa.append(sequence_statesnoa)
    # return the final arrays
    return np.array(State), np.array(Statenoa)

def put_descriptions_into_sequences(list_descrip_cam1, seq_length):
    # initialize final array to return
    Descrip = []
    # calculate the last image index to use as an initial image in the sequence
    last_index = len(list_descrip_cam1) - seq_length + 1
    for des_idx in range(0, last_index):
        # re-init the sequence, so the current sequence only include seq_length states
        sequence_des = []
        # iterate through each position in the sequence
        for spos in range(0, seq_length):
            des_num = des_idx + spos
            des = list_descrip_cam1[des_num]
            sequence_des.append(des)
        # add the current sequence to the final array
        Descrip.append(sequence_des)
    # return the final array
    return np.array(Descrip)

#
# helper function to sort a list of filenames
#  each filename has values separated by '-' and frame number is fifth element
#original_list has all the image filenames for a single run for a single campos in coll/noncoll folder
def frame_num_sort(original_list):
    # make a list with each element is a frame number
    list_fno = []
    list_descrip = []
    for element in original_list:
        eparts = element.split('-')
        list_fno.append(int(eparts[4]))     # index=4 for fifth element, frame no.
        # make frame description
        frame_descrip = eparts[0] + '-' + eparts[1] + '-' + eparts[2] + '-' + eparts[3] + '-' + eparts[4]
        list_descrip.append(frame_descrip)
    #
    # debug
    # print("list_fno: ", list_fno)
    # make new list of filenames
    pairs = zip(list_fno, original_list)
    pairs = sorted(pairs)
    newlist = [ x[1] for x in pairs ]
    # make new list of descriptions
    pairs2 = zip(list_fno, list_descrip)
    pairs2 = sorted(pairs2)
    new_descrip_list = [ x[1] for x in pairs2 ]
    return newlist, new_descrip_list

#
# helper function to return list of runs from a list of filenames
def get_runs(dircontents): #dircontents has sorted list of all image names in one of coll/Nocoll folder
    """Extract the runs from our data."""
    list_runs = []
    for item in dircontents:
        # skip if a dir
        if os.path.isdir(item):
            continue
        # skip if not a png file
        extparts = item.split('.')
        try:
            if extparts[1] != 'png':
                continue
            #
        except IndexError:
            continue
        # get the run number
        fparts = item.split('-')
        #Ex filenames 6-25-13-2-293-3, 6-30-13-2-89-2, 6-45-13-12-154-3, 6-55-13-2-182-3
        run = fparts[0] + '-' + fparts[1] + '-' + fparts[2] + '-' + fparts[3]
        if run not in list_runs:
            list_runs.append(run)
    #
    # Sort them.
    list_runs = sorted(list_runs)
    # Return.
    return list_runs

# helper function to get all filenames in a list related to a particular run
#target_run is individual run in list_runs, dircontents is the entire list of images in coll/noncoll folder
def get_filenames_for_run(target_run, dircontents):
    #
    list_filenames = []
    for item in dircontents:
        # skip if a dir
        if os.path.isdir(item):
            continue
        # skip if not a png file
        extparts = item.split('.')
        try:
            if extparts[1] != 'png':
                continue
            #
        except IndexError:
            continue
        fparts = item.split('-')
        current_run = fparts[0] + '-' + fparts[1] + '-' + fparts[2] + '-' + fparts[3]
        if current_run == target_run:
            list_filenames.append(item)
    # return
    return list_filenames
#
#
def make_npz(npzdir, imagedir, statedir, nb_states, seq_length):
    #
    #  MES
    #
    # get the list of image filenames
    dircontents = sorted(os.listdir(imagedir))
    # debug
    print("make_npz: imagedir=", imagedir)

    # make a list of the different runs in the list of filenames
    list_runs = get_runs(dircontents)
    print("For directory %s, Total number of runs is %d " % (imagedir, len(list_runs)))
    print("list_runs: ", list_runs)
    # loop thru the list of runs, getting the list of files for that run
    for run in list_runs:
        #

        print(" ")
        print("Now working on run: ", run)
        # initialize the list of files for each camera
        list_files_cam1 = []
        list_files_cam2 = []
        list_files_cam3 = []
        # get the filenames included in this run
        list_run_filenames = get_filenames_for_run(run, dircontents)
        #
        # sort the list of image filenames in order of frame number
        for filename in list_run_filenames:
            fparts = filename.split('-')
            # separate into three lists according to cam number
            if fparts[-1] == '1.png':
                list_files_cam1.append(filename)
            elif fparts[-1] == '2.png':
                list_files_cam2.append(filename)
            elif fparts[-1] == '3.png':
                list_files_cam3.append(filename)
            #
        # sort by frame number
        list_files_cam1, list_descrip_cam1 = frame_num_sort(list_files_cam1)
        list_files_cam2, list_descrip_cam2 = frame_num_sort(list_files_cam2)
        list_files_cam3, list_descrip_cam3 = frame_num_sort(list_files_cam3)
        #
        # get images in lists
        list_images_cam1 = get_images_in_list(imagedir, list_files_cam1)
        list_images_cam2 = get_images_in_list(imagedir, list_files_cam2)
        list_images_cam3 = get_images_in_list(imagedir, list_files_cam3)
        #
        # get states in lists
        list_states, list_statesnoa = get_states_in_imagefile_list(statedir, list_files_cam1)
        #
        # assemble images into sequences
        seq_images1 = put_images_into_sequences(list_images_cam1, seq_length)
        seq_images2 = put_images_into_sequences(list_images_cam2, seq_length)
        seq_images3 = put_images_into_sequences(list_images_cam3, seq_length)
        #
        # assemble states into sequences
        seq_states, seq_statesnoa = put_states_into_sequences(list_states, list_statesnoa, seq_length)
        #
        # assemble descriptions into sequences
        seq_descriptions = put_descriptions_into_sequences(list_descrip_cam1, seq_length)
        #
        #
        # set the number of examples
        num_examples = np.shape(seq_images1)[0]
        # check to make sure all sequence array have same number of examples
        if np.shape(seq_images1)[0] != num_examples:
            print("error: np.shape(seq_images1)[0] != num_examples")
            exit()
        if np.shape(seq_images2)[0] != num_examples:
            print("error: np.shape(seq_images2)[0] != num_examples")
            exit()
        if np.shape(seq_images3)[0] != num_examples:
            print("error: np.shape(seq_images3)[0] != num_examples")
            exit()
        if np.shape(seq_states)[0] != num_examples:
            print("error: np.shape(seq_states)[0] != num_examples")
            exit()
        if np.shape(seq_statesnoa)[0] != num_examples:
            print("error: np.shape(seq_statesnoa)[0] != num_examples")
            exit()
        if np.shape(seq_descriptions)[0] != num_examples:
            print("error: np.shape(seq_descriptions)[0] != num_examples")
            exit()
        #
        #
    	# write npz files
        if os.path.isdir(npzdir) == False:
            os.mkdir(npzdir)
        npz_filename = os.path.join(npzdir,run + '.npz')
        if nb_states != 0:
            # save files with sequences within 5sec of closest approach
            np.savez(npz_filename, num_examples=num_examples, \
                seq_images1=seq_images1, seq_images2=seq_images2, seq_images3=seq_images3, \
                seq_states=seq_states, seq_statesnoa=seq_statesnoa, \
                seq_descriptions=seq_descriptions)

        else:
            # within Xsec of closest approach
            np.savez(npz_filename, num_examples=num_examples, \
                seq_images1=seq_images1, seq_images2=seq_images2, seq_images3=seq_images3, \
                seq_descriptions=seq_descriptions)

    # end of for loop for each run
# end of make_npz()

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = "parser to parse all input directories for sequence_builder")

    cwd = os.getcwd()
    root_path = os.path.normpath(os.getcwd() + os.sep + os.pardir)
    imdata_path = os.path.join(root_path, "infdata")

    if os.path.isdir(imdata_path) == False or len(os.listdir(imdata_path)) == 0:
        print("the raw image data folder imdata does not exist or is empty; Please refer to webots link in readme to create/fill the folder")

    parser.add_argument('--npzdir', type=str, default=os.path.join(imdata_path, "npzdir"),
                         help="enter the npz file to store the sequences")
    parser.add_argument('--statedir', type=str, default=os.path.join(imdata_path, "State"),
                         help="folder which has the state information from webots")
    parser.add_argument('--imagedir', type=str, default=os.path.join(imdata_path, "images"),
                         help="folder that stores all the images for testing")
    parser.add_argument('--nb_states', type=int, default=17,
                         help="input for number of states extracted from webots")
    parser.add_argument('--seq_length', type=int, default=5,
                         help="input for mentioning the output sequence length or frames per sequence")

    args = parser.parse_args()
    if os.path.isdir(args.npzdir) == False:
        os.mkdir(args.npzdir)

    #
    # start the clock
    start_time = time.time()

    make_npz(args.npzdir, args.imagedir, args.statedir, args.nb_states, args.seq_length)
    #
    # stop the clock
    elap_time = time.time() - start_time
    # report the elapsed time
    print('Elapsed time: %.2f seconds' % (elap_time))

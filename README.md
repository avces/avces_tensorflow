# Deep Predictive Models for Collision Risk Assessment in Autonomous Driving
[https://arxiv.org/pdf/1711.10453.pdf](https://arxiv.org/pdf/1711.10453.pdf)

This is a tensorflow implementation of a convolutional LSTM based deep network described in the above [paper](https://arxiv.org/pdf/1711.10453.pdf) which can be used in collision avoidance systems.

Please follow the [link](https://cpslab.assembla.com/spaces/sim-atav/git/source) for the code to generate new data using webots and it is important that the specifications of data from the webots code match with the corresponding flags of this code.

## Prerequisites

The programming language used is python (tested with python3) and all the instructions are tested on Ubuntu. To use the bash scripts(./scripts/*) in windows you need to convert them to windows or mac format using unix2dos or unix2mac tool.

**Required packages**: *numpy, csv, argparse, tensorflow-gpu (with GPU support) or tensorflow (for CPU only), sklearn, scipy, scikit-image, tqdm, pillow*

You can install these packages using the requirements.txt and pip installer from the console. (Please remember to replace 'tensorflow-GPU' with 'tensorflow' in requirements.txt if you want CPU version only).
```
pip3 install -r requirements.txt
```
change pip3 to pip if you have python2.7

## Directory Structure and Descriptions

The root directory has following folders:
* **src**: This folder contains the python scripts to prepare datasets, define network model, data input pipeline and train/val/test the network.
* **scripts**: This folder contains the bash scripts to run the above python code with given input flags.
* **imdata**: This folder should contain the raw image data generated from webots simulation stored with a particular file structure. Please refer [link](https://github.com/cesardaniel92/webots) for   further details on required file structure.
* **tfrecords_dir**: This folder contains the post-processed image data in tensorflow's .tfrecords format. Refer to preparing dataset section for further details.
* **filewriter_dir**: This folder stores train and validation summaries of individual runs when the network is trained. These can be visualized using tensorboard. Refer to Summary visualization section for further details.
* **chkpt_dir**: This directory stores the intermediate checkpoints while training the network. The --run_num flag should be changed for different runs to prevent overwriting of previously stored checkpoints. You can do this via the command line argument (Ex: ```python3 src/train_val.py --run_num=5```) or you can change this in the script files(**Additional details about all flags in scripts/train_val.sh and src/flags_initialize.py files**).
* **timeline_dir**: This directory stores the chrome supported .json files which can be used for a detailed time analysis of individual operations in the network. Enter ```chrome://tracing``` in google chrome and load the .json files to observe the time profile of network. You can disable time profiling by initializing --timeline_enable flag to False (enabled by default).
* **infdata**: Contains the image data to perform inference on. Refer to preparing dataset and test/inference section.

## Preparing the Dataset

### Training and Test Data
The first step is to convert the RAW webots simulation data into sequences along with labeling and store as a .npz file(numpy compressed format). These npz files of individual sequences are combined together into tensorflow's .tfrecords format to speed up the input pipeline. This can be done using the src/sequence_builder.py , src/sequence_combiner.py and src/tfrecord_gen.py scripts. To use these scripts, the imdata directory should have a particular structure. Refer to the [link](https://github.com/cesardaniel92/webots) for supported file structure.

* The structure of the tfrecords directory where the data to input pipeline is saved.

        tfrecords_dir
            |
            |---->train/: stores the training data in .tfrecords format
            |
            |---->validate/: stores the validation data in .tfrecords format
            |
            |---->test/: stores the test data in .tfrecords format
            |
            |---->tfrecord_data_details.txt: has information about number of samples (automatically generated)
            |
            |---->tfrecord_data_coll_details.txt: has information about number of collsion and no-coll samples

**Note:** Please make sure that the same directory structure is maintained and also the names of the .txt file (if modified) should match the --tfrecord_details_file and --tfrecord_coll_info flags defined in **scripts/flags_initialize.py**. You can even change this from ./scripts/train_val bash script.
* The sequence_builder.py script extracts individual runs from the imdata folder and creates .npz files with labels for each run in the ./imdata/npzdir folder.
* The sequence_combiner.py script combines all the above generated .npz files into run_combined.npz file.
* The tfrecord_gen.py generates the train, validate and test .tfrecord files in the train, validate and test folder respectively using above generated run_combined.npz file. It also generates the .txt files described above. The default train, validation, and test split are 0.77,0.15 and 0.08 respectively. You can change the splits in ./src/tfrecord_gen.py file

**Note:** If the npzdir is at a different location specify it using the --npzdir flag of ./src/tfrecord_gen.py file or add the flag from ./scripts/prepare_dataset.sh.

From the root directory perform the steps in same order with optional flags described in individual files.
```
    python3 ./src/sequence_builder.py # give optional flags
    python3 ./src/sequence_combiner.py # give optional flags
    python3 ./src/tfrecord_gen.py # give optional flags
```
 or simply run the ```./scripts/prepare_dataset.sh``` bash script. You can add any optional flags in the script file.

**Note:** The sequence_builder and sequence_combiner steps use heavy computation and memory (at least 20GB of RAM is recommended or even more for larger data. Alternatively you can implement your own code for parallel reading and writing to improve efficiency. Also, check the seq_builder, seq_combiner and tfrecord_gen files for more flag options.

### Inference Data
Unlike the training data, the inference data is given directly form .npz file and it does not have labelling. The raw data for inference should be loaded in infdata directory. The supported file structure for the scripts is shown below

          infdata
            |
            |----> images: folder to store all the camera images for inference.
            |
            |----> State: folder to store state information for the images.
            |
            |----> npzdir: folder to store the .npz files to give as input to network.

Check the ```scripts/seq_builder_inference.py``` for flag options to change the default paths. Default seq_length is 5 and number of states is 17. Check the [link](https://github.com/cesardaniel92/webots) to generate the data with given specifications.

The ```scripts/seq_builder_inference.py``` python script reads the images in infdata and generates .npz files into infdata/npzdir folder

## Training/Validation

To perform and display training and validation results use the ```./scripts/train_val.sh``` bash script which calls the ```./src/train_val.py``` file and you can change any of the flags from the script file. All the available flags are described in the corresponding script file and also in ```./src/flags_initialize.py``` file. The checkpoints, timeline, and summaries will be written into the corresponding directories with the given --run_num. The frequency of storing summaries and checkpoints can be changed using appropriate flags described in the above scripts. Check the ```--SFP_enable``` flag to enable or disable stochastic forward passes described in the paper

From root directory run the following to start training and validation.
```
./scripts/train_val.sh
```
Check the script file to observe and change the flag defaults.

### Visualizing the Train/Val Summaries

To visualize the above-written summaries in the ./filewriter_dir folder you should use tensorboard by typing the following in the console

```
tensorboard --logdir=./filewriter_dir/run* --port=8008
```
run* is the folder name in your ./filewriter_dir corresponding to a specific run_num or visualize all the runs together with

```
tensorboard --logdir=./filewriter_dir --port=8008
```
The console displays a localhost address which you can open in any browser to visualize the network and graphs

## Test/Inference

* To **test/inference** the network you can run
```
./scripts/test_script.sh
```
This calls the ```./src/test.py``` file. In test mode the script uses the  .tfrecord data in ```./tfrecord_dir/test``` folder and it evaluates the network by using the ground truth labels. The values returned are predicted labels and ground truth labels for individual frames and accuracy of the network. Please check the bash script file for more details on the flag options.

* To run inference on RAW data without labels use the inference mode using  ```--inference``` flag (disables tfrecord and takes input from ```--inf_npzfile``` flag if True).

Please check the ```test_script.sh``` file for more details before running the script.

## Major Contributors

* **Mark Strickland** - *created the network and developed the initial version of code; refer to [paper](https://arxiv.org/pdf/1711.10453.pdf) for more technical information.*
* **Cumhur Erkan Tuncali** - *developed a general framework for webots simulation to extract raw data.*
* **Cesar Daniel** - *modified the framework for webots simulation to generate raw image data for the current task; refer to the github [link](https://github.com/cesardaniel92/webots) for more details.*
* **Sai Krishna Bashetty** - *modified, evaluated and documented the final version of the code.*

*All are from **Arizona State University** *

## Acknowledgements
This work has been co-advised by
* **Georgios Fainekos** - [public page](http://www.public.asu.edu/~gfaineko/)
* **Heni Ben Amor** - [public page](http://henibenamor.weebly.com/)

## License
